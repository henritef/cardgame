package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class representing a hand of cards
 */
public class HandOfCards extends ArrayList<PlayingCard> {

  private final ArrayList<PlayingCard> hand = new ArrayList<>();

  /**
   * Constructor that initalizes a new hand of cards
   *
   * @param hand the list of cards that make up the hand of cards.
   */
  public HandOfCards(ArrayList<PlayingCard> hand) {
    if (hand == null) {
      throw new IllegalArgumentException("Hand cannot be empty");
    }
    this.hand.addAll(hand);
  }

  /**
   * Get-method for the cards in the hand
   *
   * @return the hand of cards
   */
  public List<PlayingCard> getCards() {
    return hand;
  }

  /**
   * Method for checking if a hand is a flush,
   * i.e. 5 cards of the same suit.
   *
   * @return true if the hand is a flush, false if not
   */
  public boolean checkFlush() {
    char firstCardSuit = hand.get(0).getSuit();
    return hand.stream().allMatch(p -> p.getSuit() == firstCardSuit);
  }

  /**
   * Method for checking the hand for cards with the suit "hearts"
   *
   * @return list of all hearts in the hand
   */
  public List<PlayingCard> checkForHearts() {
    return hand.stream().filter(h -> 'H' == h.getSuit()).collect(Collectors.toList());
  }

  /**
   * Method for adding the sum of the cards in the hand
   * Ace counts as 1
   *
   * @return the sum of the cards in the hand
   */
  public int sumOfCards() {
    return hand.stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * Method for checking the if the queen of spades is in the hand
   *
   * @return true or false
   */
  public boolean checkForQueenOfSpades() {
    return hand.stream().anyMatch(card -> card.getAsString().equals("S12"));
  }

}
