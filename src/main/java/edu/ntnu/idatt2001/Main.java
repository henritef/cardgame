package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.util.List;

/**
 * Class for setting up GUI
 */
public class Main extends Application {
  private final DeckOfCards deck = new DeckOfCards();
  private HandOfCards hand;
  private boolean isChecked;

  @Override
  public void start(Stage stage) {
    stage.setTitle("Card game");
    Pane pane = new Pane();
    pane.setStyle("-fx-background-color: rgb(1, 100, 1)");

    Button dealHandBtn = new Button("Deal hand");
    dealHandBtn.setLayoutY(60);
    dealHandBtn.setLayoutX(800);
    dealHandBtn.setPrefSize(120, 40);

    Button checkHandBtn = new Button("Check hand");
    checkHandBtn.setLayoutY(200);
    checkHandBtn.setLayoutX(800);
    checkHandBtn.setPrefSize(120, 40);

    Label cardsLeftInDeck = new Label("Cards left in deck: " + deck.getDeckSize());
    cardsLeftInDeck.setTextFill(Color.WHITE);
    cardsLeftInDeck.setFont(new Font(16));
    cardsLeftInDeck.setLayoutX(800);
    cardsLeftInDeck.setLayoutY(100);

    pane.getChildren().add(cardsLeftInDeck);
    pane.getChildren().add(dealHandBtn);
    pane.getChildren().add(checkHandBtn);

    VBox vBox = new VBox();
    vBox.setLayoutY(100);
    vBox.setLayoutX(100);

    pane.getChildren().add(vBox);


    dealHandBtn.setOnAction(event -> {
      hand = new HandOfCards(deck.dealHand(5));
      cardsLeftInDeck.setText("Cards left in deck: " + deck.getDeckSize());
      displayHand(vBox);
    });
    checkHandBtn.setOnAction(event -> displayCheckHand(vBox));
    Scene scene = new Scene(pane, 1000, 600);

    stage.setResizable(false);
    stage.setScene(scene);
    stage.show();
  }

  /**
   * Method for displaying a hand of cards
   *
   * @param vBox Vertical box to display cards in
   */
  private void displayHand(VBox vBox) {
    vBox.getChildren().clear();
    HBox hBox = new HBox();
    hBox.setSpacing(10);
    for (PlayingCard card : hand.getCards()) {
      ImageView imageView = new ImageView((new Image("cards/" + card.getAsString() + ".png")));
      imageView.setFitHeight(200);
      imageView.setFitWidth(125);
      hBox.getChildren().add(imageView);
    }
    vBox.getChildren().add(hBox);
    isChecked = false;
  }

  /**
   * Method for checking hand for different checkpoints.
   *
   * @param vBox Vertical box to display information about the hand
   */
  private void displayCheckHand(VBox vBox) {
    if(isChecked) return;
    int sum = hand.sumOfCards();
    List<PlayingCard> hearts = hand.checkForHearts();
    Label sumLabel = new Label("Sum of cards: " + sum);
    Label flushLabel = new Label("Flush: " + (hand.checkFlush() ? "YES" : "NO"));
    Label queenOfSpadesLabel = new Label("Queen of spades: " + (hand.checkForQueenOfSpades() ? "YES" : "NO"));
    StringBuilder builder = new StringBuilder("Hearts: ");
    if (hearts.isEmpty()) {
      builder.append("No hearts");
    } else {
      for (PlayingCard card : hearts) {
        builder.append(card.getAsString()).append(" ");
      }
    }
    Label heartsLabel = new Label(builder.toString().trim());
    sumLabel.setTextFill(Color.WHITE);
    sumLabel.setFont(new Font(16));
    flushLabel.setTextFill(Color.WHITE);
    flushLabel.setFont(new Font(16));
    queenOfSpadesLabel.setTextFill(Color.WHITE);
    queenOfSpadesLabel.setFont(new Font(16));
    heartsLabel.setTextFill(Color.WHITE);
    heartsLabel.setFont(new Font(16));

    vBox.getChildren().addAll(sumLabel, flushLabel, queenOfSpadesLabel, heartsLabel);
    isChecked = true;
  }


  public static void main(String[] args) {
    launch(args);
  }

}
