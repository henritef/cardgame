package edu.ntnu.idatt2001;

/**
 * Class that represents an individual playing card.
 */
public class PlayingCard {
  private final char suit;
  private final int face;

  /**
   * Constructor that initializes a new playing card.
   *
   * @param suit can be spades, hearts, diamond or clubs
   * @param face can not be lower than 1 or higher than 13
   */
  public PlayingCard(char suit, int face) {
    if (!validCards(suit, face)) {
      throw new IllegalArgumentException("Invalid card");
    }
    this.suit = suit;
    this.face = face;
  }

  /**
   * Method for formatting a playing card to a String
   *
   * @return the suit and face of the playing card
   * Example: 5 of diamond = D5
   */
  public String getAsString() {
    return String.format("%s%s", suit, face);
  }

  /**
   * Get-method for the suit of the playing card
   *
   * @return the suit of the card
   */
  public char getSuit() {
    return suit;
  }

  /**
   * Get-method for the face of the playing card
   *
   * @return the face of the card
   */
  public int getFace() {
    return face;
  }

  /**
   * Method for checking if a given card is valid
   * @param suit should only be 'S', 'H', 'D' or 'C'
   * @param face should be a number between 1-13
   * @return true if card is valid, false if not
   */
  public boolean validCards(char suit, int face) {
    return (face >= 1 && face <= 13) &&
            (suit == 'S' || suit == 'H'
                    || suit == 'D' || suit == 'C');
  }

}
