package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class that represents a full deck of cards with all 52 cards.
 */
public class DeckOfCards {

  private final ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

  /**
   * Constructor that creates a new deck of cards.
   * Stores the playing cards in an arraylist.
   */
  public DeckOfCards() {
    char[] suit = {'S', 'H', 'D', 'C'};
    for(char s : suit) {
      for(int i = 1; i <= 13; i++) {
        deckOfCards.add(new PlayingCard(s,i));
      }
    }
  }

  /**
   * Method for dealing a hand of cards.
   *
   * @param n number of cards to be dealt
   * @return the cards dealt in a list
   */
  public ArrayList<PlayingCard> dealHand(int n) {
    Random random = new Random();
    ArrayList<PlayingCard> hand = new ArrayList<>();

    for(int i = 0; i < n; i++) {
      int index = random.nextInt(deckOfCards.size());
      hand.add(deckOfCards.remove(index));
    }
    return hand;
  }

  /**
   * Get-method for the deck of cards
   *
   * @return the deck of cards
   */
  public ArrayList<PlayingCard> getDeckOfCards() {
    return deckOfCards;
  }

  /**
   * Method for returning the size of the deck of cards.
   *
   * @return the number of cards in the deck.
   */
  public int getDeckSize() {
    return deckOfCards.size();
  }
}
