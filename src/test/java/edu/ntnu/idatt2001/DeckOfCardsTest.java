package edu.ntnu.idatt2001;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Deck of Cards Test")
public class DeckOfCardsTest {

  @Test
  @DisplayName("Create deck test")
  public void createDeckTest() {
    DeckOfCards deckOfCards = new DeckOfCards();
    assertEquals(52, deckOfCards.getDeckOfCards().size());
  }

  @Test
  @DisplayName("Deal hand test")
  public void dealHandTest() {
    List<PlayingCard> playingCards = new DeckOfCards().dealHand(5);
    assertEquals(5, playingCards.size());
  }
}

