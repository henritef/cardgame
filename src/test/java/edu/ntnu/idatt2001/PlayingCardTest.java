package edu.ntnu.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Playing Card Test")
public class PlayingCardTest {

  @Test
  @DisplayName("Invalid suit test")
  public void invalidSuitTest() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('A', 1));
  }

  @Test
  @DisplayName("Invalid face test")
  public void invalidFaceTest() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 0));
  }

  @Test
  @DisplayName("Get suit test")
  public void getSuitTest() {
    PlayingCard playingCard = new PlayingCard('S', 2);
    assertEquals('S', playingCard.getSuit());
  }

  @Test
  @DisplayName("Get face test")
  public void getFaceTest() {
    PlayingCard playingCard = new PlayingCard('S', 2);
    assertEquals(2, playingCard.getFace());
  }
}
